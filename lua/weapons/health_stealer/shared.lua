SWEP.PrintName = "Health stealer"
SWEP.Author = "(Pyth0n11)"
SWEP.Purpose = "Left click = Steal health (10/s) \nRight click = give health (10/s) \nReload = heal themself (10/s)"

SWEP.Spawnable = true
SWEP.ViewModel = ""
SWEP.WorldModel = ""
SWEP.Category = "Health Stealer"
SWEP.Slot = 3

SWEP.Primary.Ammo = "stolen_health"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = true

SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = true

SWEP.next_reload_time = 0.1
SWEP.last_reload_time = 0

SWEP.DrawCrosshair = false

function SWEP:Initialize()
    if not IsFirstTimePredicted() then return end
    self:SetHoldType("magic")
end

function SWEP:Deploy()
    self:SetHoldType("magic")
end

function SWEP:PrimaryAttack ()
    if (game.SinglePlayer()) then self:CallOnClient("PrimaryAttack") end -- Force the client to execute the PrimaryAttack function in singleplayer.
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.1)
    local vstart = self.Owner:EyePos() + Vector(0, 0, -10)
    self.Owner:LagCompensation(true)
    local trace = util.TraceLine({start=vstart, endpos = vstart + self.Owner:GetAimVector() * 75, filter = self.Owner})
    self.Owner:LagCompensation(false)

    ent = trace.Entity
    if not (ent:IsPlayer() or ent:IsNPC()) then return end

    if SERVER then
        ent:TakeDamage(1, self.Owner, self)
    end    
    self.Owner:SetAmmo(self.Owner:GetAmmoCount(self:GetPrimaryAmmoType()) + 1, self:GetPrimaryAmmoType())
    self:EmitSound("hl1/fvox/boop.wav", 75, math.max(ent:GetMaxHealth() / ent:Health() * 100, 25))
end

function SWEP:SecondaryAttack()
    if (game.SinglePlayer()) then self:CallOnClient("SecondaryAttack") end -- Force the client to execute the SecondaryAttack function in singleplayer.
    self.Weapon:SetNextSecondaryFire(CurTime() + 0.1)
    local vstart = self.Owner:EyePos() + Vector(0, 0, -10)
    self.Owner:LagCompensation(true)
    local trace = util.TraceLine({start=vstart, endpos = vstart + self.Owner:GetAimVector() * 75, filter = self.Owner})
    self.Owner:LagCompensation(false)

    ent = trace.Entity
    if not (ent:IsPlayer() or ent:IsNPC()) then return end
    if ent:Health() >= ent:GetMaxHealth() then return end
    if self.Owner:GetAmmoCount(self:GetPrimaryAmmoType()) <= 0 then return end
    
    ent:SetHealth(ent:Health() + 1)
    self.Owner:SetAmmo(self.Owner:GetAmmoCount(self:GetPrimaryAmmoType()) - 1, self:GetPrimaryAmmoType())
    self:EmitSound("hl1/fvox/boop.wav", 75, math.max(ent:Health() / ent:GetMaxHealth() * 100, 25))
end

function SWEP:Reload()
    if (game.SinglePlayer()) then self:CallOnClient("Reload") end -- Force the client to execute the Reload function in singleplayer.
    if CurTime() < self.last_reload_time + self.next_reload_time then return end
    
    self.last_reload_time = CurTime()
    if self.Owner:Health() >= self.Owner:GetMaxHealth() then return end
    if self.Owner:GetAmmoCount(self:GetPrimaryAmmoType()) <= 0 then return end
    
    self.Owner:SetHealth(self.Owner:Health() + 1)
    self.Owner:SetAmmo(self.Owner:GetAmmoCount(self:GetPrimaryAmmoType()) - 1, self:GetPrimaryAmmoType())
    self:EmitSound("hl1/fvox/boop.wav", 75, math.max(self.Owner:Health() / self.Owner:GetMaxHealth() * 100, 25))
end
